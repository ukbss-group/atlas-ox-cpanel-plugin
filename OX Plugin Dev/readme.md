Clone ox-branded.tar.gz and edit as needed.

Edit the install.json and add your OX URL

Edit Logo if needed

Compress back to tar.gz

```cd /var/cpanel/cpanel_plugin_generator/```

```wget https://where you added your file```
Or upload via ftp/sftp

```usr/local/cpanel/scripts/install_plugin /var/cpanel/cpanel_plugin_generator/ox-branded.tar.gz```


Notes: This could be pre-configured for clients during migration process as its fairly simple.

This could be used with ansible for larger deployments.

